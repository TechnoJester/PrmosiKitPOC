# PrmosiKitPOC

This project is proof of concept for working with Alamofire+PromiseKit+SwiftyJSON connection.
Before launching please run pod install in the peroject deirectory.

This project works with "the movie data base" API to retrieve some sample data from network. More info at https://developers.themoviedb.org/3/getting-started/introduction

This app has following main functions:
-retrieve list of trending movies for last day and show it on main screen

-show selected trending film's poster on fullscreen with animation sequence
For that tap on poster once. To close it tap on poster again.

-Search movie/collection-of-movies by name
Go to search screen by tapping on search button on top right corner of main screen

-show error alert if retrieving trending movies failes

Project structure:
The project has 3 main workgroups:
-TrendingModule - representsing first screen of the app, which contains collection view with trending movies

-SearchModule - represents search screen. It holds cell class and viewcontroller+presenter for search screen

-Network - this group is responsible for requesting data from themoviedb.org and converting  it into models

-Models - just models

Architecture is MVC for trendingModule and MVP for Search Module

This app uses libraries:
Alamofire
SwiftyJSON 
PromiseKit
SDWebImage (for downloading images)

More info on Promises here:
https://github.com/mxcl/PromiseKit/blob/master/Documentation/GettingStarted.md
https://github.com/mxcl/PromiseKit/blob/master/Documentation/CommonPatterns.md
