//
//  AddMovieTableViewCell.swift
//  MoviePicker
//
//  Created by Michael Dyachenko on 28.10.2018.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import UIKit

class MovieTableViewCell : UITableViewCell {
    var index = -1
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var originalTitleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!

    func setMedia(media: MediaModel) {
        self.titleLabel.text = media.title
        self.originalTitleLabel.text = media.originalTitle
        self.yearLabel.text = media.additinalInfo
    }
}
