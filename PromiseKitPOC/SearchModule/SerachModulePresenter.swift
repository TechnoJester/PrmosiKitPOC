//
//  SerachModulePresenter.swift
//  PromiseKitPOC
//
//  Created by Evgenii Deputatov on 11/1/18.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import Foundation
import PromiseKit
import SwiftyJSON

protocol SearchModuleViewControllerProtocol {
    func showDetails()
    func showItems(_ items : [MediaModel])
}

class SearchModulePresenter {
    private var mediaResults : [MediaModel] = []
    var viewController : SearchModuleViewControllerProtocol!
    
    func sarchFieldTextDidChange(text : String) {
        if text.isEmpty {
            mediaResults = []
            viewController.showItems(mediaResults)
            return
        }
        searchMediaItems(query: text)
    }
    
    //Promise kit using
    private func searchMediaItems(query : String) {
        firstly {
            /*note, this time we are using when(resolved:) this variant does not reject if any of included promises fails
             Instead it continues to work, even if all promises are failed.
             because of that it returns Guarantee, so no need in Catch block
             so if collections search will fail, movies search will still be available
            */
            when(resolved: MoviesDataSource().searchMovies(query: query), CollectionsDataSource().searchCollections(query: query))
        }
        .done {
            self.handleResults($0)
        }
        //no catch block
        //rewrite https://api.themoviedb.org/3/search/movie or https://api.themoviedb.org/3/search/collection to return 401 to see how it works
    }
    
    private func handleResults(_ results : [Result<[MediaModel]>] ) {
        self.mediaResults = []
        for result in results {
            switch result {
            case .fulfilled(let mediaList):
                self.mediaResults.append(contentsOf: mediaList)
            case.rejected(_):
                break
            }
        }
        self.viewController.showItems(self.mediaResults)
    }
}
