//
//  SearchModuleViewController.swift
//  MoviePicker
//
//  Created by Michael Dyachenko on 28.10.2018.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import PromiseKit

class SearchModuleViewController : UIViewController {
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var items : [MediaModel] = []
    private let presenter = SearchModulePresenter()
    
    override func viewDidLoad() {
        presenter.viewController = self
        
         searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        // Get the default Realm
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: "\(MovieTableViewCell.self)")
        
        tableView.tableFooterView = UIView()
        
        searchTextField.delegate = self
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapHandle))
        tapGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text else {
            return
        }
        presenter.sarchFieldTextDidChange(text: text)
    }
    
    @objc func tapHandle () {
        searchTextField.endEditing(true)
    }
}

extension SearchModuleViewController : UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(MovieTableViewCell.self)", for: indexPath) as! MovieTableViewCell
        let media = items[indexPath.row]
        cell.setMedia(media: media)
        cell.index = indexPath.row
        return cell
    }
}

extension SearchModuleViewController : UITableViewDelegate {
    
}
extension SearchModuleViewController : SearchModuleViewControllerProtocol {
    func showDetails() {
        
    }
    
    func showItems(_ items: [MediaModel]) {
        self.items = items
        tableView.reloadData()
    }
    
    
}

extension SearchModuleViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
