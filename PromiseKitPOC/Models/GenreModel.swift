//
//  GenreModel.swift
//  PromiseKitPOC
//
//  Created by Evgenii Deputatov on 11/1/18.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import Foundation
import SwiftyJSON

struct GenreModel {
    var id : Int
    var title : String
    
    init(_ json : JSON) {
        id = json["id"].intValue
        title = json["name"].stringValue
    }
}
