//
//  MovieMovel.swift
//  PromiseKitPOC
//
//  Created by Evgenii Deputatov on 11/1/18.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import Foundation
import SwiftyJSON

struct MovieModel : MediaModel {
    var id : Int
    var title : String
    var originalTitle : String
    var releaseDate : String
    var genresIDList : [Int] = []
    var posterPath :  String
    var additinalInfo: String
    
    init(_ json : JSON) {
        //using .intValue and .stringValue to get non-optional values (it will be just 0 and "" respectively in case of error). to get an optional, use variant without "Value" suffix, i.e. .int -> Int? and .string -> String?
        id = json["id"].intValue
        title = json["title"].stringValue
        originalTitle = json["original_title"].stringValue
        releaseDate = json["release_date"].stringValue
        posterPath = json["poster_path"].stringValue
        genresIDList = json["genre_ids"].arrayValue.map({ (genreJSON) -> Int in
            genreJSON.intValue
        })
        additinalInfo =  String(releaseDate.prefix(4))
    }
}
