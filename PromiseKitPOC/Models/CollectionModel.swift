//
//  CollectionModel.swift
//  PromiseKitPOC
//
//  Created by Evgenii Deputatov on 11/1/18.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import Foundation
import SwiftyJSON

class CollectionModel : MediaModel {
    var id : Int
    var title : String
    var originalTitle : String
    var additinalInfo: String
    
    init(_ json : JSON) {
        id = json["id"].intValue
        title = json["name"].stringValue
        originalTitle = json["original_name"].stringValue
        additinalInfo = "Collection"
    }
}
