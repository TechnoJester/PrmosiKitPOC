//
//  MediaModel.swift
//  PromiseKitPOC
//
//  Created by Evgenii Deputatov on 11/1/18.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import Foundation

protocol MediaModel {
    var id : Int {get}
    var title : String {get}
    var originalTitle : String {get}
    var additinalInfo :  String {get}
    
}
