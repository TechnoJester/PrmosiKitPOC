//
//  MovieCollectionViewCell.swift
//  PromiseKitPOC
//
//  Created by Evgenii Deputatov on 11/1/18.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import UIKit
import SDWebImage

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    
    func setMovieModel(movie : MovieModel) {
        titleLabel.text = movie.title
        genreLabel.text = GenresDataSource().getGenresNames(genreIDList: movie.genresIDList).joined(separator: ", ")
        if let url = URL(string: "https://image.tmdb.org/t/p/w500" + movie.posterPath) {
            posterImageView.sd_setImage(with: url)
        }
    }
    
    func getPosterFrame() -> CGRect {
        return posterImageView.frame
    }
    func getPosterImage() -> UIImage? {
        return posterImageView.image
    }
}
