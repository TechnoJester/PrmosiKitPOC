//
//  ViewController.swift
//  PromiseKitPOC
//
//  Created by Evgenii Deputatov on 11/1/18.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import UIKit
import Alamofire
import PromiseKit

class TrendingViewController: UIViewController {
    
    // loading indicator while fethicng trending movies
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate var moviesList : [MovieModel] = []
    
    // this property is to duplicate poster for animation (easy way)
    private var posterCopyImageView : UIImageView = UIImageView()
    
    
    //MARK: - set up
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpPosterCopyImageView()
        activityIndicator.startAnimating()
        
        // just fake delay to see activity indicator
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {[unowned self] in
            self.updateTrendingMovies()
        }
    }
    
    private func setUpPosterCopyImageView () {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapHandle))
        posterCopyImageView.addGestureRecognizer(tapGestureRecognizer)
        posterCopyImageView.isHidden = true
        posterCopyImageView.isUserInteractionEnabled = true
        self.view.addSubview(posterCopyImageView)
    }
    
    @objc func tapHandle () {
        closePoster()
    }
    
    
    //MARK: - main logic this is where promises coming in
    func updateTrendingMovies() {
        
        //firstly is syntaxic sugar it is not required
        firstly {
            
            /*note: we are using here 'when(fulfilled:)' which waits until all the promises will be resolved, and after that
             goes further. All promisess in when go concurrently
             it will fail if any of the promises will fail.
            */
            when(fulfilled: MoviesDataSource().getNewMovies(), GenresDataSource().updateGenres())
        }
        //this function is used just for debug purposes is passes result through
        .tap {
            print($0)
        }
        // done is Promise which returns Void
        .done {
            self.handleMovies($0.0)
        }
        //ensure is used for code that must always be perforemd no matter if pormise failed or not
        .ensure {
            self.activityIndicator.stopAnimating()
        }
        //catch - is the place where you can handle all the errors that could occure in any lenght promises chain. One handle for  all errors
        //if any promise in chain will fail, then remaining promises will not be performed and we go in catch block immediatelly
        // rewrite https://api.themoviedb.org/3/trending/movie/ in Charles to return error 401 to see how it works
        .catch { error in
            self.handleError(error)
        }
    }
    
    
    //MARK: - Error handling
    private func handleError(_ error : Error) {
        if let alamofireError = error as? Alamofire.AFError {
            switch alamofireError {
            case .responseValidationFailed(let validationError):
                switch validationError {
                case .unacceptableStatusCode(let code):
                    self.showAlert(errorCode: code)
                default:
                    print(alamofireError)
                }
            default:
                print(alamofireError)
            }
        }
    }
    
    private func showAlert(errorCode : Int) {
        let message : String
        switch errorCode {
        case 401:
            message = "Undathorized access"
        default:
            message = "Unknow error"
        }
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: false, completion: nil)
    }
    
    //MARK: -  handling result of fetch
    private func handleMovies(_ movies : [MovieModel]) {
        self.moviesList = movies
        self.collectionView.reloadData()
    }
    
    //MARK: - animations
    //the whole point of using promises is chaining, this allows us to perform async tasks consecutevly
    fileprivate func showPosterFullScreen (itemIndexPath : IndexPath) {
        self.createPosterCopy(itemIndexPath: itemIndexPath)
        self.view.isUserInteractionEnabled = false
        firstly {
            self.fadeOutCollectionView()
        }
        //then MUST return a promise
        .then {
            self.centerPosterCopy()
        }
        .then {
            self.scalePosterCopyToFullScreen()
        }
        .ensure {
             self.view.isUserInteractionEnabled = true
        }
    }
    
    private func closePoster () {
        self.view.isUserInteractionEnabled = false
        firstly {
            self.fadeOutPoster()
        }
        .then {
            self.fadeInCollectionView()
        }
        .ensure {
            self.view.isUserInteractionEnabled = true
        }
    }
    
    // creating copy of poster for anumations showcase
    private func createPosterCopy(itemIndexPath : IndexPath) {
        let cell = collectionView.cellForItem(at: itemIndexPath) as! MovieCollectionViewCell
        let frame = cell.getPosterFrame()
        posterCopyImageView.frame = self.view.convert(frame, from: cell)
        posterCopyImageView.image = cell.getPosterImage()
        posterCopyImageView.isHidden = false
        self.view.bringSubview(toFront: posterCopyImageView)
    }
    
    //MARK: -  wrapers. We can wrap any function on promise and use them
    //Guarantee is promise which never rejects, it's always fulfilled
    private func fadeOutCollectionView() -> Guarantee<Void> {
        //the seal object is used to resolve promise with Fulfill or reject
        return Guarantee<Void>{[unowned self] seal -> Void in
            UIView.animate(withDuration: 0.3, animations: {
                self.collectionView.alpha = 0.0
            }, completion: { (finished) in
                //at this example seal should fulfill with just Void, so using () for that
                seal(())
            })
        }
    }
    
    private func centerPosterCopy() -> Guarantee<Void> {
        return Guarantee<Void>{[unowned self] seal -> Void in
            UIView.animate(withDuration: 0.3, animations: {
                self.posterCopyImageView.center = self.view.center
            }, completion: { (finished) in
                seal(())
            })
        }
    }
    
    private func scalePosterCopyToFullScreen() -> Guarantee<Void> {
        return Guarantee<Void>{[unowned self] seal -> Void in
            UIView.animate(withDuration: 0.3, animations: {
                self.posterCopyImageView.frame = self.view.bounds
            }, completion: { (finished) in
                seal(())
            })
        }
    }
    
    private func fadeOutPoster() -> Guarantee<Void> {
        return Guarantee<Void>{[unowned self] seal -> Void in
            UIView.animate(withDuration: 0.3, animations: {
                self.posterCopyImageView.alpha = 0.0
            }, completion: { (finished) in
                self.posterCopyImageView.alpha = 1.0
                self.posterCopyImageView.isHidden = true
                seal(())
            })
        }
    }
    
    private func fadeInCollectionView() -> Guarantee<Void> {
        return Guarantee<Void>{[unowned self] seal -> Void in
            UIView.animate(withDuration: 0.3, animations: {
                self.collectionView.alpha = 1.0
            }, completion: { (finished) in
                seal(())
            })
        }
    }
    
}

//MARK: - collectionView extensions
extension TrendingViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return moviesList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(MovieCollectionViewCell.self)", for: indexPath) as! MovieCollectionViewCell
        let movie = moviesList[indexPath.row]
        cell.setMovieModel(movie: movie)
        return cell
    }
}

extension TrendingViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showPosterFullScreen(itemIndexPath: indexPath)
    }
}

