//
//  CollectionDataSource.swift
//  PromiseKitPOC
//
//  Created by Evgenii Deputatov on 11/1/18.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SwiftyJSON

let searchCollectionsPath = "/search/collection"

class CollectionsDataSource {
    private let requestManager = RequestManager()
    
    func searchCollections(query : String) -> Promise<[MediaModel]> {
        let params = JSON(["include_adult" : true, "language" : "en-US", "query" : query])
        return requestManager.get(path: searchCollectionsPath, params: params)
            .map({ (json) -> [CollectionModel] in
                return json["results"].arrayValue.map({ (collectionJson) -> CollectionModel in
                    CollectionModel(collectionJson)
                })
            })
    }
}
