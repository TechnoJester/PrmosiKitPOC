//
//  GenreDataSource.swift
//  PromiseKitPOC
//
//  Created by Evgenii Deputatov on 11/1/18.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SwiftyJSON

let genresPath = "/genre/movie/list"

class GenresDataSource {
    private let requestManager = RequestManager()
    static private var genres : [GenreModel] = []
    
    func getGenresNames (genreIDList : [Int]) -> [String] {
       return genreIDList.map { (genreID) -> String in
            GenresDataSource.genres.filter{$0.id == genreID}.first?.title ?? ""
        }
    }
    
    func updateGenres() -> Promise<Void> {
        let params = JSON()
        return requestManager.get(path: genresPath, params: params)
            .done({ json in
                GenresDataSource.genres = json["genres"].arrayValue.map({ (genreJson) -> GenreModel in
                            GenreModel(genreJson)
                        })
            })
    }
}
