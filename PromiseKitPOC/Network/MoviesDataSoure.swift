//
//  MoviesDataSoure.swift
//  PromiseKitPOC
//
//  Created by Evgenii Deputatov on 11/1/18.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SwiftyJSON

let trendMoviesPath = "/trending/movie/day"
let searchMoviesPath = "/search/movie"

class MoviesDataSource {
    private let requestManager = RequestManager()
    
    func searchMovies(query : String) -> Promise<[MediaModel]> {
        //we can wrap any value in JSON object
        let params = JSON(["include_adult" : true, "language" : "en-US", "query" : query])
        return requestManager.get(path: searchMoviesPath, params: params)
        .map({ (json) -> [MovieModel] in
            return json["results"].arrayValue.map({ (moviejson) -> MovieModel in
                MovieModel(moviejson)
            })
        })
    }
    
    func getNewMovies() -> Promise<[MovieModel]> {
        let params = JSON()
        return requestManager.get(path: trendMoviesPath, params: params)
            .map({ (json) -> [MovieModel] in
                return json["results"].arrayValue.map({ (moviejson) -> MovieModel in
                    MovieModel(moviejson)
                })
            })
    }
}
