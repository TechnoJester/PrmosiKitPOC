//
//  RequestManager.swift
//  PromiseKitPOC
//
//  Created by Evgenii Deputatov on 11/1/18.
//  Copyright © 2018 Evgenii Deputatov. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SwiftyJSON

enum RequestMagagerError : Error {
    case wrongURL
}

let baseUrlString = "https://api.themoviedb.org/3"
let apiKey = "83896133056cdcc37b13bb6354232c62"

/* this is the main class for making requests
 it uses Alamofire + Promise + swiftyJSON
 to use Alamofire+PromiseKit extensin we need to load pod 'PromiseKit/Alamofire'
 */

class RequestManager {
    func get(path : String, params : JSON) -> Promise<JSON> {
        return performRequest(path: path, method: .get, params: params)
    }
    
    func post(path : String, params : JSON) -> Promise<JSON> {
        return performRequest(path: path, method: .post, params: params)
    }
    
    private func performRequest (path : String, method : HTTPMethod, params : JSON) -> Promise<JSON>{
        var paramsWithAPIToken = params
        //adding  value to JSON requires to assign JSON object, it has initializers for any swift type, so just use JSON(value)
        paramsWithAPIToken["api_key"] = JSON(apiKey)
        guard let url = URL(string: baseUrlString + path) else {
            return Promise.init(error: RequestMagagerError.wrongURL)
        }
        
        //responseJSON() returns promise, so we can use it as promise now
        //to retrieve [String : Any] from JSON object (parameters) we use .dictionaryValue
        return Alamofire.request(url, method: method, parameters: paramsWithAPIToken.dictionaryValue).validate().responseJSON()
            //map used when we return some actual data NOT another promise
            .map { (responseJson, reponse) in
                /*wrap response in JSON if any error will occure and responseJson is empty promise will throw an error and it \
                will be nadled in catch block in promise chain*/
                return JSON(responseJson)
            }
    }
}
